using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyClasses;
using System;

namespace MyClassesTest
{
    [TestClass]
    public class FileProcessTest
    {
        public TestContext TestContext { get; set; }

        [TestMethod]
        public void FileNameDoesExist()
        {
            TestContext.WriteLine("Testing for file that exists");

            // Arrange 
            FileProcess fp = new FileProcess();
            bool result;

            // Act 
            result = fp.FileExists(@"C:\Sites\Learning\notes.txt");

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void FileNameDoesNotExist()
        {
            // Arrange 
            FileProcess fp = new FileProcess();
            bool result;

            // Act 
            result = fp.FileExists(@"C:\Sites\Learning\bogusFile.txt");

            // Assert
            Assert.IsFalse(result);

        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void FileNameNullOrEmpty_UsingAttribute()
        {
            // Arrange
            FileProcess fp = new FileProcess();

            // Act 
            fp.FileExists("");

            // Assert

        }

        [TestMethod]
        public void FileNameNullOrEmpty_UsingTryCatch()
        {
            //Arrange
            FileProcess fp = new FileProcess();

            // Act
            try
            {
                fp.FileExists("");
            }
            catch(ArgumentNullException)
            {
                return;
            }

            // Assert
            Assert.Fail("Exception not thrown");
        }
    }
}
